
//funcion anonima 
// function cargar(){
	
// 	setTimeout(
// 		function(){
// 		document.querySelector("div").innerHTML="texto";
// 	},
// 	2500
// 	);
	
// }


//funcion llave es igual que la funcion anonima de setTimeout.
function cargar (){

	setTimeout(() => {
	  document.querySelector("div").innerHTML="texto";
	}, 2500);
}



//metodo de funcion con nombre (DESACONSEJADO!!!)
// function cargar(){
	
// 	setTimeout(mostrar,2500)

// }

// function mostrar(){
// 	document.querySelector("div").innerHTML="texto";
// } 
// 
