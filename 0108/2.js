var Persona=function(padre){
	this.nombre=null;
	this.direccion=null;
	this.correoElectronico=null;

	this.getNombre=function(){
		return this.nombre;
	};
	this.setNombre=function(v){
		this.nombre=v || "";
	};
	this.getDireccion=function(){
		return this.direccion;
	};
	this.setDireccion=function(v){
		this.direccion=v || "";
	};
	this.getCorreoElectronico=function(){
		return this.correoElectronico;
	};
	this.setCorreoElectronico=function(v){
		this.correoElectronico=v || "";
	};

	this.crear=function(){
		console.log("creando")
	};

	this.borrar=function(){
		console.log("borrando")
	};

	this.enviarMensaje=function(){
		console.log("mensaje")
	};

	this.persona=function(p){
		this.setNombre(p.nombre);
		this.setDireccion(p.direccion);
		this.setCorreoElectronico(p.correoElectronico);
	};
	this.persona(padre);
};

var Cliente=function(hijo){
	this.numeroCliente=null;
	var fechaAlta=null;

	this.getNumeroCliente=function(){
		return this.numeroCliente;
	};
	this.setNumeroCliente=function(v){
		this.numeroCliente=v || 0;
	};
	this.getFechaAlta=function(){
		return fechaAlta;
	};
	this.setFechaAlta=function(v){
		fechaAlta=v || "";
	};	

	this.verFechaAlta=function(){
		return "1/1/2000";
	};


	this.cliente=function(h){
		Persona.call(this, h)
		this.setNumeroCliente(h.numeroCliente);
		this.setFechaAlta(h.fechaAlta);
	};
	this.cliente(hijo);
};

var Usuario=function(hijo){
	this.codigoUsuario=0;
	this.nombre=null;

	this.getCodigoUsuario=function(){
		return this.codigoUsuario;
	};
	this.setCodigoUsuario=function(v){
		this.codigoUsuario=v || 0;
	};	
	

	this.autorizar=function(){
		console.log("autorizar")
	};
	this.crear=function(){
		console.log("creando2")
	};

	this.usuario=function(h){
		this.setCodigoUsuario(h.codigoUsuario); //aqui no se puede utilizar el call ya que se queda con la propiedad del hijo y no puedo acceder a la del padre
		this.persona(h);
	};
	this.usuario(hijo);
};
Usuario.prototype=new Persona({});

var objeto1=new Persona({
	nombre:"Daniel",
	direccion:"avd Valdecilla",
	correoElectronico:"holamundo@gmail.com"
});

var objeto2=new Cliente({
	nombre:"Ragnar",
	direccion:"avd Valdemoro",
	correoElectronico:"holamundo1@gmail.com"
});

var objeto3=new Usuario({
	nombre:"Rolo",
	direccion:"avd La iglesia",
	correoElectronico:"holamundo2@gmail.com"
});


console.log(objeto1);
console.log(objeto2);
console.log(objeto3);
