// crear un array
var frutas = ['Manzana', 'Banana'];

console.log(frutas.length);

// acceder a elementos de un array

var primero = frutas[0];
// Manzana

var ultimo = frutas[frutas.length - 1];
// Banana


// recorrer un array



for(c=0;c<frutas.length;c++){
	console.log(frutas[c] , c);
}

// Manzana 0
// Banana 1

//con for in
for(c in frutas){
	console.log(frutas[c],c);

}

// Manzana 0
// Banana 1

frutas.forEach(function (elemento, indice, array) {
    console.log(elemento, indice);
});
// Manzana 0
// Banana 1


// añadir elementos al final del array
var nuevaLongitud = frutas.push('Naranja');
// ["Manzana", "Banana", "Naranja"];


// eliminar elementos al final del array

var ultimo = frutas.pop(); // elimina Naranja del final
// ["Manzana", "Banana"];


// elimina elementos al principio del array

var primero = frutas.shift(); // elimina Manzana del inicio
// ["Banana"];


//añade elementos al principio del array

var nuevaLongitud = frutas.unshift('Fresa'); // añade al inicio
// ["Fresa", "Banana"];


// encontrar el indice de un elemento del array

frutas.push('Mango');
// ["Fresa", "Banana", "Mango"];

var pos = frutas.indexOf('Banana');
// 1


// eliminar elementos de un array desde una posicion determinada

var vegetales = ['Repollo', 'Nabo', 'Rábano', 'Zanahoria'];
console.log(vegetales); 
// ["Repollo", "Nabo", "Rábano", "Zanahoria"]

var pos = 1, n = 2;

var elementosEliminados = vegetales.splice(pos, n);
// así es como se eliminan elementos, n define la cantidad de elementos a eliminar,
// de esa posicion(pos) en adelante hasta el final del array.

console.log(vegetales); 
// ["Repollo", "Zanahoria"] (el array original ha cambiado)

console.log(elementosEliminados); 
// ["Nabo", "Rábano"]

// crear una copia del array

var copiaSuperficial = frutas.slice(); // esta es la forma de crear una copia

console.log(copiaSuperficial);
// ["Fresa", "Banana", "Mango"];

copiaSuperficial[0]="Pera";
console.log(copiaSuperficial);
// ["Pera", "Banana", "Mango"];

console.log(frutas);
// ["Fresa", "Banana", "Mango"];


// crear una referencia del array
var copia=frutas;

console.log(copia);
// ["Fresa", "Banana", "Mango"];

copia[0]="Peras";
console.log(copia);
// ["Peras", "Banana", "Mango"];

console.log(frutas);
// ["Peras", "Banana", "Mango"];
