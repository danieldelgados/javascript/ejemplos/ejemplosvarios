/**
 Crear clases
 */

//var Caja=function(){// primer caracter debe ir en mayuscula

//};

var Persona=function(padre){
	var sup=this;//se utliza para poder acceder a una propiedad o metodo publico desde un metodo privado;
	//propiedades privadas
	var nombre;
	var apellidos;
	var fechaNacimiento;
	var direccion;
	//metodos publicos
	this.getNombre=function(){
		return nombre.toUpperCase();
	};
	this.setNombre=function(valor){
		nombre=valor || "";
	};
	this.getApellidos=function(){
		return apellidos.toUpperCase();
	};
	this.setApellidos=function(valor){
		apellidos=valor||"";
	};
	this.getNombreCompleto=function(){
		var aux=this.getNombre()+ " " + this.getApellidos();
		return aux;
	}
	this.getFechaNacimiento=function(){
		return fechaNacimiento;
	};
	this.setFechaNacimiento=function(valor){
		fechaNacimiento=valor|| "1/1/2000";
	};
	this.getDireccion=function(){
		return direccion;
	};
	this.setDireccion=function(valor){
		direccion=valor|| "";
	};
	this.getEdad=function(){
		var hoy=new Date();
		var year=hoy.getFullYear();
		var vector=this.getFechaNacimiento().split("/");
		var n=new Date(vector[2],vector[1],vector[0]);
		var cumple=n.getFullYear();
		var aux= year-cumple;
		return aux;
	};
	this.dibujar=function(){

	};

	
	//funcion constructora se ejecuta nada mas instanciar la clase y puede ser privada o publica , por lo general tiene que ser privada
	
	this.persona=function(valores){
		this.setNombre(valores.nombre);// error ,no puedes acceder a elementos públicos desde un metodo privado.
		this.setApellidos(valores.apellidos);
		this.setFechaNacimiento(valores.fechaNacimiento);
		this.setDireccion(valores.direccion);//todo lo que queremos que se haga por defecto tiene que venir aqui.
	}
	this.persona(padre);
};

var Trabajador=function(hijo){
	//propiedades privadas
	
	var sueldos;
	var hijos;
	var empresa;
	var fechaEntrada;

	//metodos publicos
	this.getSueldo=function(){
		return sueldo;
	};
	this.setSueldo=function(v){
		// if(typeof(sueldo)=="undefined"){
		// 	sueldo=v||0;
		// }else if(typeof(v)!="undefined"){
		// 	sueldo=v;
		// }; reglas de negocio para evitar undefined y dejar todas las opciones limitadas
		 
		sueldo=v||0;
		
	};
	this.getEmpresa=function(){
		return empresa;
	};
	this.setEmpresa=function(v){
		empresa=v || "";
	};
	this.getHijos=function(){
		return hijos;
	};
	this.setHijos=function(v){
		hijos=v||0;
	};
	this.getFechaEntrada=function(){
		return fechaEntrada;
	};
	this.setFechaEntrada=function(v){
		fechaEntrada=v||"1/1/2000"
	};
	this.dibujar=function(){

	};

	//metodo constructor
	this.trabajador=function(valores){
		this.persona(valores);
		this.setSueldo(valores.sueldo);
		this.setHijos(valores.hijos);
		this.setEmpresa(valores.empresa);
		this.setFechaEntrada(valores.fechaEntrada);
	};
	this.trabajador(hijo);
	
};
Trabajador.prototype=new Persona({});

/**
 
 **/
var Empresa=function(valores){
	var nombre;
	var direccion;

	this.getNombre=function(){
		return nombre;
	};
	this.setNombre=function(v){
		nombre=v;
	};
	this.getDireccion=function(){
		return direccion;
	};
	this.setDireccion=function(v){
		direccion=v;
	};
	this.dibujar=function(){

	};

	this.empresa=function(){

	};
	this.empresa();

}
// instanciar clase

var jose=new Persona({// aqui creamos el JSON
	nombre:"jose",
	apellidos:"vazquez rodriguez",
	fechaNacimiento:"24/11/1990",
	direccion:"Avd Valdecilla 11 ,Santander"

});
console.log(jose.getNombreCompleto());
console.log(jose.getEdad());


var silvia=new Trabajador({
	sueldo:1000,
	hijos:5,
	empresa:"Google",
	fechaEntrada:"1/1/2015",
	nombre:"Silvia"
});

console.log(silvia.getEmpresa());
console.log(silvia.getSueldo());
console.log(silvia.getNombre());

/*
sobrecarga:es tener dos metodos con el mismo nombre

sobreescribir:es tener un metodo en el hijo que se llama igual que el del padre()prevalece el del hijo).
 */


