var Forma=function(padre){
	this.area=0;

	this.getArea=function(){
		return this.area;
	};
	this.setArea=function(v){
		this.area=v || 0 ;
	};

	this.forma=function(p){
		this.setArea(p.area);
	};
	this.forma(padre);
};


var Cuadrado=function(hijo){
	this.lado=0;

	this.getLado=function(){
		return this.lado;
	};
	this.setLado=function(v){
		this.lado=v || 0;
	};
	this.cuadrado=function(h){
		//this.forma(h);
		this.setLado(h.lado);
		Forma.call(this,h);
	};
	this.cuadrado(hijo);
};
//Cuadrado.prototype=new Forma({});

objeto=new Forma({area:100});
objeto1=new Cuadrado({
	lado:10,
	area:200
});

console.log(objeto);
console.log(objeto1);