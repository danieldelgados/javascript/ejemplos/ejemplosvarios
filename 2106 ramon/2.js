function calcularSuperficie(r){
		
	// calcular la superficie
	s=Math.PI*Math.pow(r,2);

	return s;
}


function calcularPerimetro(ra){
	
	return (2*Math.PI*ra);

}


function leer(objeto){
	return document.querySelector(objeto).value;
}

function calcular(tipo){

	var r=0;
	var resultado=0;

	r=leer('#radio');
	
	if(tipo=='superficie'){
		resultado=calcularSuperficie(r);
		mostrar(resultado,"#superficie");
	}else{
		resultado=calcularPerimetro(r);
		mostrar(resultado,"#perimetro");
	}

	mostrar(resultado,".salida",'div');
	dibujar(r);
}

function mostrar(valor,objeto,tipo='input'){
	if(tipo=='input'){
		document.querySelector(objeto).value=valor;
	}else{
		document.querySelector(objeto).innerHTML=valor;
	}
}

function dibujar(rad){
	var escala=20;
	var diametro=0;

	diametro=rad*escala;

	document.querySelector(".dibujo").style.width=diametro +"px";
	document.querySelector(".dibujo").style.height=diametro +"px";
}