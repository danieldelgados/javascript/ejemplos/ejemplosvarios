var n1=1;
var n2=2;
var n3=3;
var n4=4;
var salida1=0;
var salida2=0;
var salida3=0;
var salida4=0;

function funcion1(arg1,arg2){
	var suma=0;
	suma=arg1+arg2;
	return suma;
}

function funcion2(arg1,arg2,arg3){
	var suma=0;
	suma=arg1+arg2+arg3;
	return suma;
	
}

function funcion3(arg1,arg2,arg3,arg4){
	var suma=0;
	suma=arg1+arg2+arg3+arg4;
	return suma;

}

function funcion4(arg1,arg2){
	var multiplicar=0;
	multiplicar=arg1*arg2;
	return multiplicar;
}
//asi llamamos para que hagan algo a las funciones para que muestren lo que queremos

salida1=funcion1(n1,n2);
document.write("<div>" + "Suma de la funcion 1 es " + salida1 + "</div>");

salida2=funcion2(n1,n2,n3);
document.write("<div>" + "Suma de la funcion 2 es " + salida2 + "</div>");

salida3=funcion3(n1,n2,n3,n4);
document.write("<div>" + "Suma de la funcion 3 es " + salida3 + "</div>");

salida4=funcion4(n1,n2);
document.write("<div>" + "Multiplicación de la funcion 4 es " + salida4 + "</div>");
