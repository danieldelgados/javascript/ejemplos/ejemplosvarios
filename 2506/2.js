//variables globales
var n=0;
var sPositivos=0;
var sNegativos=0;
var nTotal=0;

function entrada(){
	//variable local
	var numero=0;
	//introducir datos
	n=parseInt(prompt("Introduce un número"));
	//devolver lo que se obtiene en la funcion asi que cada vez que se ejecute la funcion entrada se puede guardar en la variable global
	return numero; 
}

/* Esta función me permite mostrar un mensaje y una variable dentro de un alert
** mensaje : es el mensaje a mostrar
** variable : es la variable a mostrar
 */
function salida(mensaje,variable){
	document.write("<p>");
	document.write(mensaje);
	document.write(variable);
	document.write("</p>");

	// document.write("<p>" + mensaje + variable + "</p>");
}
// funcionde salida con la variación de una lista
function salida1(mensaje,variable){

	document.write("<li>" + mensaje + variable + "</li>");
}
 
function salida2(mensaje,variable,etiqueta){

	document.write("<"+ etiqueta +">" + mensaje + variable + "</" + etiqueta + ">");

}

 num=entrada();



while(n!=0){//condicion del bucle
	if (n>0) {//condicion de los argumentos dentro del bucle no puede ser lo mismo que el bucle
	sPositivos+=n;		
	} else {
	sNegativos+=n;
	}
	nTotal++;// todo lo que sea comun para if and else tiene que estar dentro del while pero fuera de estos mismos
	num=entrada();//no olvidar que esto mantiene el bucle 
}

// esto se ejecuta una vez de terminar el bucle o si la condicion del bucle es falsa
//mostrar resultados
//
salida("La suma de los positivos es ",sPositivos);
salida("La suma de los negativos es ",sNegativos);
salida("El total de numeros utilizados es ",nTotal);

salida1("La suma de los positivos es ",sPositivos);
salida1("La suma de los negativos es ",sNegativos);
salida1("El total de numeros utilizados es ",nTotal);

salida2("La suma de los positivos es ",sPositivos,"h1");
salida2("La suma de los negativos es ",sNegativos,"li");
salida2("El total de numeros utilizados es ",nTotal,"div");

