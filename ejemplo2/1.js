// funcion sin argumentos y no devuelve nada
function nombreCompleto(){
	console.log("nombre","apellido1","apellido2");
}

// llamar a la funcion

nombreCompleto();

// funcion con argumentos y no devuelve nada
function nombreCompleto1(nombre,apellido1,apellido2){
	console.log("nombre",nombre,"apellido1",apellido1,"apellido2",apellido2);
}


//llamar a la funcion
nombreCompleto1("ramon","abramo","feijoo");
var n="ramon";
var apellido1="abramo";

nombreCompleto1(n,apellido1,"apellido1");

// funcion con argumentos y que devuelve algo
function nombreCompleto2(nombre,apellidos){
	return nombre + " " + apellidos;
}

// llamar a la funcion
var salida=nombreCompleto2("ramon","abramo feijoo");

console.log("salida",salida);
console.log("nombre",nombreCompleto2("ramon","abramo feijoo"));

// funcion con argumentos y devuelve algo

function square(number) {
  return number * number;
}

// llamar a la funcion
square(2);

var r=square(3);
console.log("r",r);

console.log("salida",square(2));

// funcion con argumentos por defecto
function sumar(a,b=0,c=0){
	return a+b+c;
}

console.log("argumentos1",sumar(1,2,3));
console.log("argumentos2",sumar(1,2));
console.log("argumentos3",sumar(1));
console.log("argumentos4",sumar());


// utilizacion del array arguments para argumentos variables
function sumarv(){
	var c=0;
	var a=0;
	for(c=0;c<arguments.length;c++){
		a+=arguments[c];
	}

	return a;
}


console.log("argumentosVariables1",sumarv(1,2,3,4,5));
console.log("argumentosVariables2",sumarv(1,2));
console.log("argumentosVariables3",sumarv());


// utilizacion de argumentos rest
function sumarr(a=0,...b){
	var c=0;
	var suma=a;
	for(c=0;c<b.length;c++){
		suma+=b[c];
	}

	return suma;	
}

console.log("argumentosRest1",sumarr(1,2,3,4));
console.log("argumentosRest2",sumarr());



// funcion declarada
function uno(){
	return 1;
}

// funcion expresada (funcion anonima, funcion lambda)
var funcion1=function(){
	return 1;
}; //esta funcion no se podria llamar antes de su creacion


console.log("uno",uno());
console.log("funcion1",funcion1());


// funcion autoejecutable
console.log(function(){
		var c=0;
		var r="";
		for(c=0;c<10;c++){
			r+=c;
		}
		return r;

	}());


