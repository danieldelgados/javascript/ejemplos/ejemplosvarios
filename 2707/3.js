
//creación de la clase
var Caja=function(){
	var sup=this;//acceder a cualquier miembro(propiedad) pública desde un metodo privado.
	var unidad="px";//propiedad privada
 // elementos publicos
	this.ancho=500;
	this.alto=50;
	this.color="red";
	this.contenido="";
	// metodo privado
	
	function concatenar(arg){
		return arg +unidad;
 	}

 // metodos publico
 	
 	this.setAncho=function(v){//siempre lleva un argumento 
 		if(v>=0 && v<=1000){
 		this.ancho=v;
 		}
 	}

 		
 	this.getAncho=function(){//siempre lleva un return
 		return concatenar(this.ancho);
 	}

 	this.setAlto=function(v){//siempre lleva un argumento 
 		if(v >= 0 && v >= 200){
 		this.ancho=v;
 		}
 	}

 	this.getAlto=function(){//siempre lleva un return
 		return concatenar(this.alto);
 	}

 	this.dibujarCaja=function(donde){
 		var c=document.createElement("div");
 		c.style.width=this.getAncho();
 		c.style.height=this.getAlto();
 		c.style.backgroundColor=this.color;
 		c.innerHTML=this.contenido;
 		document.querySelector(donde).appendChild(c);

 	};
}
window.addEventListener("load",()=>{
	var caja1=new Caja();

	caja1.ancho=600;
	caja1.setAncho(600);//la idea es utilizar ésta forma ya que asi no cambian los valores en parametros desconocidos 

	console.log(caja1.ancho);
	console.log(caja1.getAncho());

	caja1.dibujarCaja(".salida");

	caja1.contenido="esto es un ejemplo de objetos";
	caja1.dibujarCaja(".otro");	

	var caja2=new Caja();
	caja2.setAncho(800);
	caja2.setAlto(20);
	caja2.color="blue";
	caja2.contenido="Esto es más texto";

	caja2.dibujarCaja("body");
});
