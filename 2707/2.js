// crear clases con funciones!!!! es lo que se utiliza hasta la fecha.(functions)
		// solo las ultimas actualizaciones de los navegadores soportan las clases de forma nativa(class).

		/**
		 crear una clase
		 */
		
		var Caja=function(){
			var sup=this;//creo esta variable para que se pase la clase completa y así los elemntos privados pueden acceder a los públicos 

			//propiedad pública.
			this.ancho=500;// esto es una propiedad, accesible fuera del elemento, es pública.
			// propiedad privada
			var unidad="px";// esto en realidad es una variable local que se utiliza dentro de la funcion.

			//metodo publico()accesible desde fuera (this)
			this.crearCaja=function(){
				console.log(concatenar());
			}
			// metodo privado
			
			function concatenar(){
				return sup.ancho+unidad;// cuando se utiliza un metodo privado no puedo acceder a elementos públicos.
			}
		}
		//así se simula una clase, y las clases tienen elementos. 
/*
1- primero debo crear la clase
2- instanciar la clase!!!
3- ya se obtiene  un objeto
 */

 		//instanciar una clase
 		
 		var caja1=new Caja();//esta es una caja
 		var caja2=new Caja();//otra caja utilizando el molde Caja(clase)

 		caja1.crearCaja();

 		caja2.ancho=100;

 		caja1.crearCaja();
 		caja2.crearCaja();

 		//las clases tienen->
 		/**miembros//
 		-privado
 		-publico
 		**/ 
 		/**
 		 los miembros pueden ser propiedades y metodos
 		 */
 		
 		/**
 		 cuando creas el objeto solo puedes acceder a miembros publicos.
 		 */
		/**
		 setters
		 getters
		 constructora
		 */